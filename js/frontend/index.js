import './slider'
import './animation'
import './popup'
import './scroll'
import './filter'
import './ajax'
import './map'
import './anchor'
import './tooltip'
import './form'
import './accordion'
import './dropdown'

$(document).ready(function () {
    let linkTab = $(".js-tab-link > div"),
        contentTab = $(".js-tab-content > div"),
        elClosest = 'body';

    linkTab.on("click", function(e) {
        let $this = $(this),
            tabActive = $this.closest(elClosest).find('.js-tab-link');

        tabActive.find('div').removeClass("active");
        $this.addClass("active");
        let clickedTab = tabActive.find('.active');

        contentTab.removeClass("active");
        let clickedTabIndex = clickedTab.index();
        console.log(clickedTabIndex)
        contentTab.eq(clickedTabIndex).addClass("active");
    });

    let cardMaterialSelect = $('.b-detail__material');
    cardMaterialSelect.on('click', '.js-select-items', function(){
       let $this = $(this);
       let dropdown = $this.parent().find('.card-material__select-dropdown');
        dropdown.toggleClass('active');
	});

	cardMaterialSelect.on('click', '.card-material__select-dropdown > span', function(){
        let $this = $(this);
        let textColor = $this.text();
        let select = $this.closest('.card-material__select').find('.card-material__select-wrap span:last-child');
        select.text(textColor);
	});


/*
    let showSelect = $('.js-select-items');
    let selectItem = $('.card-material__select-dropdown > span');

    showSelect.on('click', function () {
       let $this = $(this);
       let dropdown = $this.parent().find('.card-material__select-dropdown');

        dropdown.toggleClass('active');
    });

    selectItem.on('click', function () {
        let $this = $(this);
        let textColor = $this.text();
        let select = $this.closest('.card-material__select').find('.card-material__select-wrap span:last-child');
        select.text(textColor);
    });
*/
    $(document).click(function(event) {
        if ($(event.target).closest('.js-select-items').length ) return;

        $('.card-material__select-dropdown').removeClass('active');
    });


    $('.js-show-city').on('click', function () {
            $('.b-header__select-city').toggleClass('active')
    });

  if ($('.b-header__select-city').attr('data-display_flag') == 'true') {
    $('.b-header__select-city').addClass('active')
  }

    $(document).click(function(event) {
        if ($(event.target).closest('.b-header__select-city').length ) return;
        if ($(event.target).closest('.js-show-city').length ) return;

        $('.b-header__select-city').removeClass('active');
    });

  $('.b-header__select-city-bg').on('click', function () {
      $('.b-header__select-city').removeClass('active');
  });

    $(' .b-download__wrap a').click(function(e){
        e.preventDefault();
        var $this = $(this),
            tabgroup = '#'+$this.parents('.tabs').data('tabgroup'),
            others = $this.closest('.b-download__list').find('a'),
            target = $this.attr('href');
        others.removeClass('active');
        $this.addClass('active');

        $('.b-download__right').children('div').removeClass('active');
        $(target).addClass('active');

    });

    $('.js-show-client').on('click', function () {
        let $this = $(this);
        if ($this.prev().hasClass('b-blog__client--active-4')) {
            $this.prev().removeClass('b-blog__client--active-6')
            $this.prev().removeClass('b-blog__client--active-4')
            $this.text('Показать еще');
            return
        }
        if ($this.prev().hasClass('b-blog__client--active-6')) {
            $this.prev().addClass('b-blog__client--active-4')
            $this.text('СКРЫТЬ');
            return
        }
        $this.prev().addClass('b-blog__client--active-6')
    });

    let scrollTop = $('#js-scrolltop');

    scrollTop.on('click', function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });

    let el = document.getElementsByTagName('body')[0]

    $(window).on('scroll', function() {
        let rect = el.getBoundingClientRect().top;
        if (rect >= -300) {
            scrollTop.removeClass('active')
            return
        }
        scrollTop.addClass('active')

    });


})
