$(document).ready(function () {
    var hashTagActive = "";
    $(".js-anchor").on("click touchstart" , function (event) {
        if(hashTagActive != this.hash) { //this will prevent if the user click several times the same link to freeze the scroll.
            event.preventDefault();
            //calculate destination place
            var dest = 0;
            if ($(this.hash).offset().top > $(document).height() - $(window).height()) {
                dest = $(document).height() - $(window).height();
            } else {
                dest = $(this.hash).offset().top;
            }
            //go to destination
            $('html,body').animate({
                scrollTop: dest
            }, 1000, 'linear');
            hashTagActive = this.hash;
        }
    });

  let el = document.getElementById('sub-scroll');
  let lastScrollTop = 0;

  if (el) {
    $(window).on('scroll', function() {
      let rect = el.getBoundingClientRect().top;
      let st = $(this).scrollTop();

      if (st > lastScrollTop){
        // downscroll code
        $('.js-scroll-sub-menu').removeClass('scroll')
        $('.js-anchor--last').removeClass('show')
      } else {
        // upscroll code
        if (rect >= 0) {
          $('.js-scroll-sub-menu').removeClass('scroll')
          $('.js-anchor--last').removeClass('show')
          return
        }
        $('.js-scroll-sub-menu').addClass('scroll')
        $('.js-anchor--last').addClass('show')


      }


      lastScrollTop = st;
    });
  }
});
