require("jquery-mousewheel");
require('malihu-custom-scrollbar-plugin');

$(document).ready(function () {
    $('.js-submit').on('click', function (e) {
        e.preventDefault()
        var test = $('form').serialize();
    });


    $('.b-select-filter__checkbox label').on('click', function () {
        let $this = $(this);
        let filter = $this.closest('.b-select-filter__item').find('input');

        let itemFilter = $this.closest('.b-select-filter__item');
        let checkbox = $this.prev();

        checkbox.toggleClass('js-active');
        itemFilter.removeClass('checked');

        filter.each(function (index, item) {
            if ($(item).hasClass('js-active')) {
                itemFilter.addClass('checked');
            }
        });
        let text = $this.text();
        let item = $this.closest('.b-select-filter__item').find('.b-select-filter__select');
        let textDefault = item.attr('data-default');

        if (!itemFilter.hasClass('checked')) {
            item.text(textDefault);
        } else {
            item.text(text);
        }


    });

    $('.b-select-filter__close').on('click', function () {
        let $this = $(this);
        let item = $this.closest('.b-select-filter__item');
        let checkbox = item.find('input');
        let select = item.find('.b-select-filter__select');
        let textDefault = select.attr('data-default');

        select.text(textDefault);
        item.removeClass('checked');
        checkbox.each(function (index, item) {
            $(item).removeClass('js-active');
            $(item).prop('checked', false);
        });
    });

    $('.b-select-filter__block').on('click', function () {
        let $this = $(this);
        let dropdown = $this.next();
        let dropdownWrap = dropdown.find('.b-select-filter__dropdown-wrap');
        let dropdownScroll = dropdown.find('.b-select-filter__dropdown-sroll');
        let heightDropdown = dropdownScroll.height();

        if ($this.next().hasClass('active')) {
            $this.next().removeClass('active');
            $this.parent().removeClass('active');
            return
        }
        $('.b-select-filter__item').each(function (index, item) {

            if ($(item).hasClass('active')) {
                $(item).find('.b-select-filter__dropdown').removeClass('active')
                $(item).removeClass('active')
            }
        });

        dropdown.toggleClass('active');
        $this.parent().toggleClass('active');

        if (heightDropdown > 200) {
            dropdownWrap.mCustomScrollbar({
                scrollButtons: { enable: false },
                scrollbarPosition: "outside",
                mouseWheel: { preventDefault: true }
            });
        }
        let rs = 10;
        console.log(rs);
    });

    $(document).click(function(event) {
        if ($(event.target).closest('.b-select-filter__item').length ) return;
        if ($(event.target).closest('.b-select-filter__close').length ) return;

        $('.b-select-filter__item').removeClass('active');
        $('.b-select-filter__dropdown').removeClass('active');
    });
});
