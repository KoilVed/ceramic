$(document).ready(function() {
    $('body').on('click', '.js-accordion', function(e) {
        if (window.matchMedia('(max-width: 1024px)').matches) {
            let elem = $(this);
            let list = elem.find('ul');

            if (!elem.hasClass('open')) {
                let heightElem = elem.outerHeight();
                let heightList = list.outerHeight();
                let height = heightElem + heightList;
                elem.addClass('open');
                elem.css('height', `${height}px`);
                list.slideDown(300);
            } else {
                elem.removeClass('open');
                elem.css('height', '');
                list.slideUp(300);
            }
        }
    });

    $('body').on('click', '.js-accordion ul', function(e) {
        if (window.matchMedia('(max-width: 1024px)').matches) {
            e.stopPropagation();
        }
    });
});