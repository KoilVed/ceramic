let skrollr = require('skrollr');
//
// if( ($(window).innerWidth()) <= 1025) {
//     skrollr.init().destroy();
// } else {
//
// }
$(document).ready(function() {
    if (window.matchMedia('(min-width: 1280px)').matches) {
        skrollr.init({
            smoothScrolling: false,
            render         : function(data) {
                // document.querySelector(".whereami").innerHTML = data.curTop;
            }
        });
    }
    $('.js-anim-header').on('click', function() {
        let $this = $(this);
        let parent = $this.closest('.b-main');
        let headerTop = $('.b-header__top');
        let headerBottom = $('.b-header__bottom');
        headerTop.toggleClass('active')
        headerBottom.toggleClass('active')
        parent.toggleClass('active')
    });
});
