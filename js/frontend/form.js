require('inputmask/dist/inputmask/inputmask.numeric.extensions');
let Inputmask = require('inputmask/dist/inputmask/inputmask.date.extensions');
require("jquery-mousewheel");
require('malihu-custom-scrollbar-plugin');

const API = '/send.php';
$(document).ready(function () {
  let mask = $('.phone-mask');

  var im = new Inputmask("+7-999-999-99-99", {
    showMaskOnHover: false
  });
  im.mask(mask);


  $('.js-form-sub').on('submit', function (e) {
    e.preventDefault()
    let form_serialized = $(this).serialize();

    $.ajax({
      type: 'POST',
      url: API,
      data: form_serialized,
      complete: function (data) {

        $.magnificPopup.open({
          items: {
            src: '#js-popup-sub'
          },
          type: 'inline',
          fixedContentPos: true,
          removalDelay: 300,
          mainClass: 'custom-mfp mfp-fade',
          showCloseBtn: false,
          callbacks: {
            open: function () {
              $('.js-custom-scroll').mCustomScrollbar();

            },
            beforeClose: function () {

            }
          }
        });
      }
    });

    let $mfp = $.magnificPopup.instance;
    $(document).on('click', '.b-popup-sub__close', function(e) {
      $mfp.close();
    });

    $('.js-submit-sub').on('submit', function (e) {
      e.preventDefault();

      let form_serialized = $(this).serialize();

      $.ajax({
        type: 'POST',
        url: API,
        data: form_serialized,
        complete: function (data) {
          $.magnificPopup.open({
            items: {
              src: '#js-popup-sub-success'
            },
            type: 'inline',
            fixedContentPos: true,
            removalDelay: 300,
            mainClass: 'custom-mfp mfp-fade',
            showCloseBtn: false,
            callbacks: {
              open: function() {
                $('.js-custom-scroll').mCustomScrollbar();

              },
              beforeClose: function() {

              }
            }
          });
        }
      });
    });
  });
});
