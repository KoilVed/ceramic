require("jquery-mousewheel");
require('malihu-custom-scrollbar-plugin');

let magnificPopup = require('magnific-popup');

$(document).ready(function () {
    $('.js-scroll-table').mCustomScrollbar({
        axis: "x",
    });

    $(document).on('click', '.js-popup', function (e) {

        $(this).magnificPopup({
            fixedContentPos: true,
            removalDelay: 300,
            mainClass: 'custom-mfp mfp-fade',
            showCloseBtn: false,
            callbacks: {
                open: function () {
                    $('.js-custom-scroll').mCustomScrollbar();
                },
                beforeClose: function () {

                }
            }
        }).magnificPopup('open');

        let $mfp = $.magnificPopup.instance;

        $('.b-popup__close').on('click', function (e) {
            $mfp.close();
        });

    });


    $(document).on('click', '.js-show-text', function (e) {
        e.stopPropagation()
        $('.b-form__text').fadeToggle();
    });

    $('.js-popup-block').magnificPopup({
        type: 'image'
        // other options
    });

    $('.js-zoom-img').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        },
        closeOnContentClick: true,
        closeBtnInside: false,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom b-product-gallery__popup', // class to remove default margin from left and right side
        image: {
            verticalFit: true
        },
        zoom: {
            enabled: true,
            duration: 300 // don't foget to change the duration also in CSS
        },
        callbacks: {
            buildControls: function () {
                // re-appends controls inside the main container
                this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
            }

        }
    });

    $('.js-zoom-color').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        },
        closeOnContentClick: true,
        closeBtnInside: false,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom b-product-gallery__popup b-product-gallery__popup--color', // class to remove default margin from left and right side
        image: {
            verticalFit: true,
            titleSrc: function(item) {
                return item.el.attr('title');
            }
        },
        zoom: {
            enabled: true,
            duration: 300 // don't foget to change the duration also in CSS
        },
        callbacks: {
            buildControls: function () {
                // re-appends controls inside the main container
                this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
            }
        }
    });

    $(document).on('click', 'b-product-gallery__popup figcaption', function() {
        $.magnificPopup.close();
    });


    $(document).on('click', '.b-color__item p', function () {
        let $this = $(this).prev().click();
    });

    $('.b-block__copy a').magnificPopup({
		type: 'iframe',
		fixedContentPos: true,
		removalDelay: 300,
		mainClass: 'custom-mfp mfp-fade'
    });
    
    // open tablet catalog's dropdown:
    function openModal (src, mainClass, removalDelay, callbacks = {}) {
        $.magnificPopup.open({
            items: {
                src
            },
            fixedContentPos: true,
            overflowY      : 'auto',
            showCloseBtn   : false,
            removalDelay,
            alignTop : true,
            mainClass,
            preloader: false,
            callbacks: {
                open       : callbacks.open ? callbacks.open : null,
                beforeClose: callbacks.beforeClose ? callbacks.beforeClose : null
            }
        });
    }

    $('body').on('click', '.js-modal-catalog', function(e) {
        e.preventDefault();
        if (window.matchMedia('(max-width: 1024px)').matches) {
            let elem = $(this);
            let src = elem.attr('href');
            let mainClass = 'custom-mfp-catalog mfp-fade';
            let removalDelay = 300;
            let callbacks = {};
            callbacks.open = function() {
                let parent = elem.parents();
                let closeBtn = $.magnificPopup.instance.content.find('.b-modal__close');
                let offset = parent.offset();
                if (!parent.hasClass('close')) {
                    parent.addClass('close');
                }
                closeBtn.css('left', `${offset.left}px`);
            };
            callbacks.beforeClose = function() {
                let parent = elem.parents();
                let mfp = $.magnificPopup.instance;
                let items = mfp.content.find('.b-modal__list > li');
                if (parent.hasClass('close')) {
                    parent.removeClass('close');
                }
                items.each(function() {
                    let item = $(this);
                    let sublist = item.find('.b-modal__sublist');
                    if (item.hasClass('close')) {
                        item.removeClass('close');
                        sublist.css('display', 'none');
                    }
                });
            };

            openModal(src, mainClass, removalDelay, callbacks);
        }
    });

    // close tablet catalog's dropdown:
    $('body').on('click', '.js-modal-close', function() {
        $.magnificPopup.close();
    });

    // open tablet catalog's subdropdown:
    $('body').on('click', '.b-modal__list li', function() {
        let el = $(this);
        let sublist = el.find('.b-modal__sublist');
        if (sublist.length !== 0) {
            sublist.slideToggle(300);
            el.toggleClass('close');
        }
    });

    // open tablet menu:
    $('body').on('click', '.js-modal-tb-menu', function(e) {
        e.preventDefault();
        if (window.matchMedia('(max-width: 1024px)').matches) {
            let elem = $(this);
            let src = elem.attr('href');
            let mainClass = 'custom-mfp-tb-menu';
            let removalDelay = 700;
            let callbacks = {};

            openModal(src, mainClass, removalDelay, callbacks);
        }
    });

    // close tablet menu:
    let startX = 0;

    $('body').on('touchstart', '.custom-mfp-tb-menu', function(e) {
        startX = e.originalEvent.touches[0].clientX;
    });

    $('body').on('touchend', '.custom-mfp-tb-menu', function(e) {
        let finishX = e.originalEvent.changedTouches[0].clientX;
        if (finishX > startX) {
            $.magnificPopup.close();
        }
    });

    // gallery certificates
    $('.js-zoom-certificates').magnificPopup({
        type: 'image',
        delegate: 'a',
        gallery: {
            enabled: true
        },
        closeOnContentClick: true,
        closeBtnInside: true,
        fixedContentPos: true,
        mainClass: 'custom-mfp-cerfs',
        image: {
            verticalFit: true
        },
        zoom: {
            enabled: true,
            duration: 300 
        },
        callbacks: {
            // buildControls: function () {
            //     // re-appends controls inside the main container
            //     this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
            // }
        }
    });
});
