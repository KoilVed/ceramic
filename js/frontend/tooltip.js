$(document).ready(function () {


    let windowWidth = $(window).innerWidth() / 2;
    const posTriangle = 42;
    let loginBtn = $('.js-tooltip');
    let loginBlock = $('.b-tooltip');
    let closetooltip = $('.b-tooltip__close');

    let widthLoginBlock = loginBlock.innerWidth();
    let heightLoginBlock = loginBlock.innerHeight();

    loginBtn.hover(function (event) {
        let $this = $(this);
        let title = $this.attr('data-title-tooltip');
        let text = $this.attr('data-text-tooltip');

        loginBlock.find('.b-tooltip__title').text(title)
        loginBlock.find('.b-tooltip__text').text(text)

        let heightBtn = $this.innerHeight();
        let centerPos = posTriangle - heightBtn / 2;
        let top = $this.offset().top - heightBtn + 25;
        let left = $this.offset().left + 25;

        loginBlock.addClass('active');
        loginBlock.css({
            'top': `${top}px`,
            'left': `${left}px`
        });
    }, function () {
        loginBlock.removeClass('active');
    });

    closetooltip.on('click', function () {
        loginBlock.removeClass('active');
    });

    // $(document).click(function(event) {
    //     if ($(event.target).closest('.js-tooltip').length ) return;
    //     if ($(event.target).closest('.b-tooltip').length ) return;
    //
    //     loginBlock.removeClass('active');
    // });

    // скролл по мышки
    // $('.b-product-table__content > div').mousewheel(function (event, delta) {
    //     this.scrollLeft -= (delta * 40);
    //     event.preventDefault()

    // })
});