$(document).ready(function() {
    $('body').on('click', '.js-dropdown-head', function(e) {
        if (window.matchMedia('(max-width: 1024px)').matches) {
            let parent = $(this).parents('.js-dropdown').first();
            let dropdown = parent.children('.js-dropdown-content');
            let allDropdowns = parent.find('.js-dropdown-content');

            if (!parent.hasClass('open')) {
                parent.addClass('open');
                dropdown.slideDown(300);
            } else {
                parent.removeClass('open');
                dropdown.slideUp(300, function() {
                    allDropdowns.each(function() {
                        let parent = $(this).parents('.js-dropdown').first();
                        if (parent.hasClass('open')) {
                            parent.removeClass('open');
                            $(this).css('display', 'none');
                        }
                    });
                });
            }
        }
    });
});