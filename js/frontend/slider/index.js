import slick from 'slick-carousel';
import 'owl.carousel';

let paramsHomeSlider = {
    dots    : false,
    infinite: true,
    speed   : 500,
    fade    : true,
    cssEase : 'linear'
};

let paramsFilterSlider = {
    infinite     : true,
    speed        : 300,
    slidesToShow : 1,
    swipeToSlide : true,
    variableWidth: true,
    centerMode   : true,
    initialSlide : 2
};

let paramsProductSlider = {
    centerMode   : true,
    centerPadding: '310px',
    slidesToShow : 1,
    appendArrows : $('.b-product-slider__arrow'),

    responsive: [
        {
            breakpoint: 1025,
            settings  : {
                centerMode  : false,
                slidesToShow: 1,
                fade        : true,
                appendArrows: $('.b-product-slider__arrow')
            }
        }
    ]
};
let paramsProductSliderv2 = {
    centerMode   : true,
    centerPadding: '310px',
    slidesToShow : 1,
    appendArrows : $('.b-product-slider__arrow-v2'),

    responsive: [
        {
            breakpoint: 1025,
            settings  : {
                centerMode  : false,
                slidesToShow: 1,
                fade        : true,
                appendArrows: $('.b-product-slider__arrow-v2')
            }
        }
    ]
};

let paramsBlogSlider = {
    dots    : false,
    infinite: true,
    speed   : 500,
    fade    : true,
    cssEase : 'linear'
};

let paramsContactsSlider = {
    dots        : false,
    slidesToShow: 9,
    speed       : 500,
    infinite    : false
};

let paramsCategorySlider = {
    dots        : false,
    infinite    : true,
    speed       : 300,
    slidesToShow: 6,

    responsive: [
        {
            breakpoint: 1025,
            settings  : {
                dots        : false,
                infinite    : true,
                speed       : 300,
                slidesToShow: 4,
                arrows      : false,
              swipeToSlide : true
            }
        },
        {
            breakpoint: 481,
            settings  : {
                centerMode   : true,
                variableWidth: true,
                dots         : false,
                infinite     : true,
                speed        : 300,
                arrows       : false,
                swipeToSlide : true
            }
        }
    ]
};

let paramsCertificatesSlider = {
    infinite      : true,
    speed         : 300,
    slidesToShow  : 3,
    slidesToScroll: 3
};

let homeSlider = $('.js-home-slider');
let topFilterSlider = $('.js-top-slider-filter');
let productSlider = $('.js-product-slider');
let productSliderv2 = $('.js-product-slider-v2');
let blogSlider = $('.js-blog-slider');
let productSliderSingle = $('.js-product-slider-single');
let contactsSlider = $('.js-slider-contacts');
let categorySlider = $('.js-slider-category');
let certificatesSlider = $('.js-certificates-slider');

$(document).ready(function() {
    homeSlider.slick(paramsHomeSlider);
    productSliderSingle.slick(paramsHomeSlider);
    topFilterSlider.slick(paramsFilterSlider);
    // productSlider.slick(paramsProductSlider);
    blogSlider.slick(paramsBlogSlider);
    contactsSlider.slick(paramsContactsSlider);
    categorySlider.slick(paramsCategorySlider);
    certificatesSlider.slick(paramsCertificatesSlider);

    topFilterSlider.on('click', '.slick-slide', function(e) {
        e.stopPropagation();
        var index = $(this).data('slick-index');
        if (topFilterSlider.slick('slickCurrentSlide') !== index) {
            topFilterSlider.slick('slickGoTo', index);
        }
    });

    // $('.owl-carousel').owlCarousel({
    //     margin:64,
    //     loop:true,
    //     autoWidth:true,
    //     items:9
    // })
});

window.addEventListener('load', function() {
    productSlider.slick(paramsProductSlider);
    productSliderv2.slick(paramsProductSliderv2);
});


