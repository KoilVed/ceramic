import './city'
let magnificPopup = require('magnific-popup');
// //dev.ceramicgroup.ru

const API = '//dev.ceramicgroup.ru/do.php';
var mgf = $.magnificPopup.instance;

class Ajax {
    constructor(filter, content, btn, reset, submit, mobile) {
        this.filter = $(filter);
        this.content = $(content);
        this.btnMore = $(btn);
        this.reset = $(reset);
        this.btnSubmit = $(submit);
        this.isMobile = mobile;
        this.inputHidden = this.content
        this.checkbox = this.filter.find('.b-filter__checkbox');
        this.checkboxInput = this.checkbox.find('input');
        this.checkboxInput = this.filter.find('input');
        this.serializ = '';
        this.serializQuery = '';
        this.isShowMore = false;
        this.lastNumberInQueryURL = 10;
        this.isGoods = false;
        this.api = API;
        this.isModeItems = '?mode=item';
        this.isModeGallery = '?mode=photo';
        this.countItems = null;
        this.isQueryRequst = false;
        this.distanceFromBottomToStartLoad = 800;
        this.loadAjaxScroll = false;
        this.insertPreloader = this.content.parent();
        this.preloaderHTML = `<div class="preloader"><div></div><div></div></div>`;
        this.showPreloader = false;
        this.firstLoad = false;
        this.noClickGallery = false;
        this.isGoods = true;
        this.isGoods = true;
        this.isFirstLoadPage = true;
        this.dataLastNumber = this.content.attr('data-last');
    }

    urlPage(param) {
        let url
        if (param == '') {
            // url = window.location.pathname
        } else {
            if (!(param.indexOf('last') == '-1')) {
                param = param.slice(0, -8);
            }
            if (param == '') {
                url = `${param}`
            } else {
                url = `?${param}`
            }
        }

        window.history.pushState(null, null, url);
    }

    initPage() {
        let self = this
        mgf = $.magnificPopup.instance;
        this.initFilterForm();
        this.scroll();
        this.insertPreloader.append(this.preloaderHTML)
        this.reset.click(() => {
            this.isQueryRequst = false;
            this.isShowMore = false;
            this.filter.removeClass('hidden-checkbox');
            this.checkbox.removeClass('active');
            this.checkboxInput.prop('checked', false);
            // this.setNumberElement('');
            self.dataLastNumber = 12
            this.serializ = '';
            this.load(this.serializ);
            window.history.pushState(null, null, window.location.href.split("?")[0]);
        });

        this.checkboxInput.on('input', function () {
            this.isQueryRequst = false;
            this.countItems = null;
            self.isShowMore = false
            self.dataLastNumber = 12
            // self.setNumberElement('');
            let form_serialized = self.filter.find('form').serialize();
            self.serializ = form_serialized;
            if (!self.isMobile) {
              self.loadItemFilter(form_serialized);
            }
        });

        this.btnSubmit.on('click', function () {
          if (self.isMobile) {
            self.loadItemFilter(self.serializ);
            $('.js-dropdown-content').slideUp(300);
            $('.js-dropdown').removeClass('open')
          }
        });

        $('.b-gallery--card').on('click', function () {
            self.noClickGallery = true
        })
    }

    setNumberElement(number) {
        this.inputHidden.attr('data-last', number)
    }

    initFilterForm() {
        this.filter.submit((e) => {
            this.isQueryRequst = false;
            e.preventDefault();
            this.countItems = null;
            this.isShowMore = false;
            this.loadElements();
        });
    }

    scroll() {
        $(window).scroll(() => {
            if (($(window).scrollTop() + $(window).height() > $(document).height() - this.distanceFromBottomToStartLoad) && !this.loadAjaxScroll) {
                this.loadAjaxScroll = true;
                this.showMore();
            }
        });
    }

    loadFilterItemPage(mode) {
        this.initPage();
        this.loadElements(mode)
    }

    loadElements(mode) {
        let form_serialized;

        if (mode === 'item') {
            this.api = this.api + this.isModeItems
        }
        if (mode === 'photo') {
            this.api = this.api + this.isModeGallery
        }

        if (this.countItems == null) {
            form_serialized = this.filter.find('form').find('input[type="checkbox"]').serialize();
        } else {
            form_serialized = this.filter.find('form').serialize();
        }
        this.serializ = form_serialized;

        if (!this.isQueryRequst) {
            this.load(form_serialized);
        } else {
            this.loadWithQuery(this.serializQuery);
        }

    }

    // запрос для обновления пунктов фильтров
    loadItemFilter(data, param = true) {
        let self = this;
        let decode = function (s) {
            return decodeURIComponent(s.replace(/\+/g, " "));
        };
        if (param) {
            // let withoutLast = decode(data).substring(0, decode(data).length - 6)
            // console.log(withoutLast.length);
            // if (withoutLast.length === 0) {
            //     withoutLast = ' '
            // }
            // console.log(withoutLast);
            //
            this.urlPage(decode(data));
        }
        this.insertPreloader.find('.preloader').addClass('active');

        $.ajax({
            type: 'GET',
            url: self.api,
            data: data,
            success: this.successRequestItem.bind(this)
        });
    }
    successRequestItem(value) {
        let data = JSON.parse(value);
        let itemFilter = data.filter;

        this.checkbox.removeClass('active');
        this.filter.addClass('hidden-checkbox');

        let objItemFilter = Object.keys(itemFilter);
        objItemFilter.forEach(function (item, i) {
            let el = objItemFilter[i] + '[]';
            for (var key1 in itemFilter[item]) {
                let valueFilter = itemFilter[item][key1].value;
                $(`input[name="${el}"][value="${valueFilter}"]`).parent().addClass('active')
            }
        });

        setTimeout(() => {
            let listItem = data.item;
            let isGoods = data.is_goods;

            if (listItem == null) {
                listItem = []
            }

            let array = [];

            Object.keys(listItem).forEach(function (item, i) {
                let el = listItem[item].html;
                array.push(el);
            });

            if (array.length === 0) {
                array = `<div class="b-not-found">товар не найден</div>`;
            }
            this.insertPreloader.find('.preloader').removeClass('active');
            if (!this.isShowMore) {
                this.content.html(array);
            } else {
                this.content.append(array);
            }
                // запрос на обновление пунктов фильтрации
            //this.initGallery()

            if (this.isQueryRequst) {
                this.countItems = this.lastNumberInQueryURL + array.length;
                this.lastNumberInQueryURL = this.countItems
            } else {
                this.countItems = this.content.children().length;
            }


            if (isGoods) {
                this.loadAjaxScroll = false
                this.dataLastNumber = this.countItems
                // this.setNumberElement(this.countItems);
            } else {
                this.loadAjaxScroll = true
                this.isShowMore = false;
                this.dataLastNumber = 12
                // this.setNumberElement('');
            }
        }, 250)
    }

    initGallery() {
        let self = this;
        $('.b-gallery--card .js-gallery-card').magnificPopup({
            type: 'image',
            mainClass: 'b-product-gallery__popup',
            gallery: {
                enabled: true
            },
            callbacks: {
                buildControls: function () {
                    // console.log('magnificInInitGallery')
                    // re-appends controls inside the main container
                    this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
                },
                change: function () {
                    let indexImg = Number(this.index) + 1;
                    let allImgs = this.items.length;
                    // console.log('init' + allImgs);
                    if (indexImg === allImgs && self.isGoods) {
                        self.loadAjaxScroll = true;
                        self.showMore()
                    }
                },
            }
        });
    }

    // запрос через submit и кнопку показать еще
    load(data, param = true) {
        let self = this;
        let decode = function (s) {
            return decodeURIComponent(s.replace(/\+/g, " "));
        };
        if (param) {
            this.urlPage(decode(data));
        }
        this.insertPreloader.find('.preloader').addClass('active');
        $.ajax({
            type: 'GET',
            url: self.api,
            data: data,
            success: this.successRequest.bind(this)
        });
    }
    successRequest(value) {
        setTimeout(() => {
            let data = JSON.parse(value);
            let itemFilter = data.filter;
            let listItem = data.item;
            let isGoods = data.is_goods;

            if (listItem == null) {
                listItem = []
            }

            this.checkbox.removeClass('active');

            this.filter.addClass('hidden-checkbox');
            let objItemFilter = Object.keys(itemFilter);

            objItemFilter.forEach(function (item, i) {
                let el = objItemFilter[i] + '[]';

                for (var key1 in itemFilter[item]) {
                    let valueFilter = itemFilter[item][key1].value;

                    $(`input[name="${el}"][value="${valueFilter}"]`).parent().addClass('active')
                }
            });
            let array = [];

            Object.keys(listItem).forEach(function (item, i) {
                let el = listItem[item].html;
                array.push(el);
            });

            if (array.length === 0) {
                array = `<div class="b-not-found">товар не найден</div>`;
            }
            this.insertPreloader.find('.preloader').removeClass('active');
            if (!this.isShowMore) {
                this.content.html(array);
            } else {
                this.content.append(array);
            }
            this.isGoods = isGoods;

            //this.initGallery();
            let self = this
            $('.b-gallery--card .js-gallery-card').magnificPopup({
                type: 'image',
                mainClass: 'b-product-gallery__popup',
                gallery: {
                    enabled: true
                },

                callbacks: {
                    buildControls: function () {
                        // console.log('magnificInInitGallery')
                        // re-appends controls inside the main container
                        this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
                    },
                    change: function () {
                        let indexImg = Number(this.index) + 1;
                        let allImgs = this.items.length;
                        // console.log('test' + allImgs);
                        if (indexImg === allImgs && self.isGoods) {
                            self.loadAjaxScroll = true;
                            self.showMore()
                        }
                    },
                }
            });


            if (this.firstLoad && this.noClickGallery) {
                Object.keys(listItem).forEach(function (item, i) {
                    let el = listItem[item].html;
                    let link = $(el).find('.js-gallery-card').attr('href')
                    let title = $(el).find('.link').attr('title')


                    mgf.items.push({
                        src: `${link}`,
                        title: title
                    });

                });
            }

            this.firstLoad = true

            if (this.isQueryRequst) {
                this.countItems = this.lastNumberInQueryURL + array.length;
                this.lastNumberInQueryURL = this.countItems
            } else {
                this.countItems = this.content.children().length;
            }


            if (isGoods) {
                this.loadAjaxScroll = false
                this.dataLastNumber = this.countItems
                // this.setNumberElement(this.countItems);
            } else {
                this.loadAjaxScroll = true
                this.isShowMore = false;
                this.dataLastNumber = 12
                // this.setNumberElement('');
            }

        }, 250)
    }

    // запрос с параметрами
    loadFilterWithQuery(mode) {
        this.isQueryRequst = true;
        this.mergeObj()
        this.initPage();
        this.loadElements(mode);
    }
    loadWithQuery(data, param = true) {
        let self = this;
        if (param) {
            this.urlPage(data);
        }
        this.insertPreloader.find('.preloader').addClass('active');
        $.ajax({
            type: 'GET',
            url: self.api,
            data: data,
            success: this.successRequestWithQuery.bind(this)
        });
    }
    successRequestWithQuery(value) {
        let data = JSON.parse(value);
        let itemFilter = data.filter;
        let listItem = data.item;
        let isGoods = data.is_goods;

        if (listItem == null) {
            listItem = []
        }

        this.checkbox.removeClass('active');

        this.filter.addClass('hidden-checkbox');
        let objItemFilter = Object.keys(itemFilter);

        objItemFilter.forEach(function (item, i) {

            let el = objItemFilter[i] + '[]';


            for (var key1 in itemFilter[item]) {
                let valueFilter = itemFilter[item][key1].value;

                $(`input[name="${el}"][value="${valueFilter}"]`).parent().addClass('active')
            }
        });
        let array = [];

        Object.keys(listItem).forEach(function (item, i) {
            let el = listItem[item].html;
            array.push(el);
        });

        if (array.length === 0) {
            array = `<div class="b-not-found">товар не найден</div>`;
        }
        this.insertPreloader.find('.preloader').removeClass('active');
        if (!this.isShowMore) {
            this.content.html(array);
        } else {
            this.content.append(array);
        }
        this.isGoods = isGoods
        this.initGallery()

        if (this.firstLoad) {
            Object.keys(listItem).forEach(function (item, i) {
                let el = listItem[item].html;
                let link = $(el).attr('href')
                let title = $(el).attr('title')
                mgf.items.push({
                    src: `${link}`,
                    title: title
                });
            });
        }
        this.firstLoad = true
        if (this.isQueryRequst) {
            this.countItems = this.lastNumberInQueryURL;
        } else {
            this.countItems = this.content.children().length;
        }

        if (isGoods) {
            this.loadAjaxScroll = false
            this.dataLastNumber = this.countItems
            // this.setNumberElement(this.countItems);
        } else {
            this.loadAjaxScroll = true
            this.isShowMore = false;
            this.dataLastNumber = 12
            // this.setNumberElement('');
        }
    }
    mergeObj() {
        let self = this;
        let serializ = [];
        let checkedfilterItems = {};
        let result = this.getQueryObj();
        let obj = Object.keys(this.getQueryObj());

        obj.forEach(function (item, i) {
            let str = '';
            if (result[item].length === 1) {
                str = item + '=' + result[item] + '&';
                if (item == 'last') {
                    self.lastNumberInQueryURL = Number(result[item]) + 10;
                    $(`input[name="${item}"]`).val(Number(result[item]) + 10);
                } else {
                    self.lastNumberInQueryURL = 10;
                    $(`input[name="${item}"][value="${result[item]}"]`).prop("checked", true);
                }
                serializ.push(str);
            } else {
                let innerArr = result[item]
                innerArr.forEach(function (_item, i) {
                    str = item + '=' + _item + '&';
                    $(`input[name="${item}"][value="${_item}"]`).prop("checked", true);
                    serializ.push(str)
                });
            }
        });

        this.serializQuery = serializ.join('').substring(0, serializ.join('').length - 1);

    }
    getQueryObj() {
        let params = location.search.substr(1).split('&'),
            results = {};
        let decode = function (s) {
            return decodeURIComponent(s.replace(/\+/g, " "));
        };

        for (var i = 0; i < params.length; i++) {
            var temp = params[i].split('='),
                key = temp[0],
                val = temp[1];

            results[decode(key)] = results[decode(key)] || [];
            results[decode(key)].push(decode(val));
        }

        return results;
    }

    // запрос на показать еще
    showMore() {

        let lastParam;
        if (this.dataLastNumber <= 12) {
            lastParam = '&last=12'
        } else {
            lastParam = `&last=${this.dataLastNumber}`
        }

        this.isShowMore = true;

        let form_serialized = this.filter.find('form').serialize() + `${lastParam}`;

        this.load(form_serialized);
    }
}

class AjaxMore {
    constructor(content, btn) {
        this.content = $(content);
        this.wrap = this.content.find('.b-product-gallery__content');
        this.dataCatalogId = this.content.attr('data-catalog-id');
        this.dataCatalogItemId = this.content.attr('data-catalog-item-id');
        this.dataLastNumber = this.content.attr('data-last');
        this.btnMore = $(btn);
        this.api = API;
        this.preloader = this.content.find('.preloader');
        this.showPreloader = false;
        this.isGoods = true
        this.initPage();
    }

    initPage() {
        let self = this;

        $('.js-gallery').magnificPopup({
            type: 'image',
            mainClass: 'mfp-no-margins b-product-gallery__popup',
            gallery: {
                enabled: true
            },
            callbacks: {
                buildControls: function () {
                    // re-appends controls inside the main container
                    this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
                },
                change: function () {
                    let indexImg = Number(this.index) + 1;
                    let allImgs = this.items.length;

                    if (indexImg === allImgs && self.isGoods) {
                        self.showMore()
                    }
                },

            }
        });

        this.btnMore.click((e) => {
            this.showMore();
        });
    }

    isCatalogId() {
        return !this.dataCatalogId == ''
    }

    isCatalogItemId() {
        return !this.dataCatalogItemId == ''
    }

    setNumberElement(number) {
        this.content.attr('data-last', number);
    }

    // запрос на показать еще
    showMore() {
        this.dataLastNumber = this.content.attr('data-last');

        let lastParam;
        if (this.dataLastNumber <= 4) {
            lastParam = '&last=4'
        } else {
            lastParam = `&last=${this.dataLastNumber}`
        }
        let form_serialized;
        this.isShowMore = true;
        if (this.isCatalogId()) {
            form_serialized = `mode=series_photo&catalog_id=${this.dataCatalogId}${lastParam}`;
        }
        if (this.isCatalogItemId()) {
            form_serialized = `mode=item_photo&catalog_item_id=${this.dataCatalogItemId}${lastParam}`;
        }

        this.load(form_serialized);
    }

    load(data) {
        let self = this;
        this.preloader.addClass('active');
        this.btnMore.addClass('active');
        $.ajax({
            type: 'GET',
            url: self.api,
            data: data,
            success: this.successRequest.bind(this)
        });
    }

    successRequest(value) {
        let self = this;
        setTimeout(() => {
            let data = JSON.parse(value);
            let itemFilter = data.filter;
            let listItem = data.item;
            let isGoods = data.is_goods;

            if (listItem == null) {
                listItem = []
            }

            this.isGoods = isGoods

            if (!isGoods) {
                this.btnMore.css('visibility', 'hidden');
            } else {
                this.btnMore.css('visibility', 'visible');
            }

            let array = [];

            Object.keys(listItem).forEach(function (item, i) {
                let el = listItem[item].html;
                array.push(el);
            });

            if (array.length === 0) {
                this.btnMore.css('visibility', 'hidden');
                array = `<div class="b-not-found">товар не найден</div>`;
            }
            this.preloader.removeClass('active')
            this.btnMore.removeClass('active')

            this.wrap.append(array);
            this.countItems = this.wrap.children().length;

            $('.js-gallery').magnificPopup({
                type: 'image',
                mainClass: 'b-product-gallery__popup',
                gallery: {
                    enabled: true
                },
                callbacks: {
                    buildControls: function () {
                        // re-appends controls inside the main container
                        this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
                    },
                    change: function () {
                        let indexImg = Number(this.index) + 1;
                        let allImgs = this.items.length;

                        if (indexImg === allImgs && isGoods) {
                            self.showMore()
                        }
                    },
                }
            });

            Object.keys(listItem).forEach(function (item, i) {
                let el = listItem[item].html;
                let link = $(el).attr('href')
                let title = $(el).attr('title')
                mgf.items.push({
                    src: `${link}`,
                    title: title
                });
            });

            if (isGoods) {
                this.loadAjaxScroll = false
                this.setNumberElement(this.countItems);
            } else {
                this.loadAjaxScroll = true
                this.isShowMore = false;
                this.setNumberElement('');
            }
        }, 250);
    }
}

class AjaxHimGoods {
    constructor(content, menu, catalog_id='') {
        this.content = $(content);
        this.menu = $(menu);
        this.wrap = this.content.find('.b-detail__card-material');
        this.dataCatalogId = catalog_id;
        this.dataLastNumber = 0;
        this.api = API;
        this.preloader = this.content.find('.preloader');
        this.showPreloader = false;
        this.isGoods = true
        this.initPage();
        this.distanceFromBottomToStartLoad = 800;
    }

    initPage() {
        let self = this;
		this.scroll();
        this.menu.on('click', '[data-catalog_id]', function(){
            if ($(this).data('catalog_id') != self.dataCatalogId){
				self.wrap.html('');
				self.menu.find('div').removeClass('active');
				$(this).addClass('active');
				self.isGoods = true;
				self.setNumberElement('');
				self.dataCatalogId = $(this).data('catalog_id');
				self.showPreloader = true;
				self.showMore();
				window.location.hash = '#'+$(this).data('catalog_url');
			}
        });
        let im = this.menu.find('[data-catalog_url="'+window.location.hash.slice(1)+'"]');
        if (im.length == 0) im = this.menu.find('[data-catalog_id]').first();
		im.click();
    }

    isCatalogId() {
        return !this.dataCatalogId == ''
    }

    setNumberElement(number) {
        this.content.attr('data-last', number);
    }


    scroll() {
        $(window).scroll(() => {
			console.log('sroll');
            if (($(window).scrollTop() + $(window).height() > $(document).height() - this.distanceFromBottomToStartLoad) && !this.showPreloader) {
                this.showPreloader = true;
                console.log('scroll-ust');
                this.showMore();
            }
        });
    }

    // запрос на показать еще
    showMore() {
        this.dataLastNumber = this.content.attr('data-last');

        let lastParam;
        if (this.dataLastNumber < 12) {
            lastParam = '&last=0'
        } else {
            lastParam = `&last=${this.dataLastNumber}`
        }
        if (this.isCatalogId()) {
            let form_serialized = `mode=him_goods&catalog_id=${this.dataCatalogId}${lastParam}`;
            this.load(form_serialized);
        }

    }

    load(data) {
        let self = this;
        this.preloader.addClass('active');
        $.ajax({
            type: 'GET',
            url: self.api,
            data: data,
            success: this.successRequest.bind(this)
        });
    }

    successRequest(value) {
        let self = this;
        setTimeout(() => {
            let data = JSON.parse(value);
            let itemFilter = data.filter;
            let listItem = data.item;
            let isGoods = data.is_goods;

            if (listItem == null) {
                listItem = []
            }

            this.isGoods = isGoods

            let array = [];

            $.each(data.item, function(i, item){
                let el = `<div class="card-material">`;
				el += `<div class="card-material__wrap">`;
				el += `<div class="card-material__img"><a href="${window.location.pathname}${item.catalog_url}/${item.catalog_item_url}/" title="${item.catalog_item_name}"><img src="/img/catalog_item/icon/${item.catalog_item_id}.jpg"></a></div>`;
				el += `<div class="card-material__text"><a href="${window.location.pathname}${item.catalog_url}/${item.catalog_item_url}/" title="${item.catalog_item_name}">${item.catalog_item_name}<p>${item.catalog_item_info}</p></a></div>`;
				el += `</div>`;
				if (item.colors.length > 0){
					el += `<div class="card-material__footer"><div class="card-material__select"><div class="card-material__select-wrap js-select-items"><span>Цвет:</span><span>${item.colors[0].element_feature_value}</span></div><div class="card-material__select-dropdown">`;
					$.each(item.colors, function(j, color){
						el += `<span>${color.element_feature_value}</span>`;
					});
					el += `</div></div>`;
				}else{
					el += `<div class="card-material__footer"><div class="card-material__select"><div class="card-material__select-wrap js-select-items"></div><div class="card-material__select-dropdown"></div></div>`;
				}
				if (item.fasovka.length > 0){
					el += `<div class="card-material__select"><div class="card-material__select-wrap js-select-items"><span>${item.fasovka[0].element_feature_value}</span></div><div class="card-material__select-dropdown">`;
					$.each(item.fasovka, function(j, fas){
						el += `<span>${fas.element_feature_value}</span>`;
					});
					el += `</div></div>`;
				}else{
					el += `<div class="card-material__select"><div class="card-material__select-wrap js-select-items"><span>${item.fasovka[0].element_feature_value}</span></div><div class="card-material__select-dropdown"></div></div>`;
				}
				el += `</div></div>`;
                array.push(el);
            });

            if (array.length === 0) {
                array = `<div class="b-not-found">товар не найден</div>`;
            }
            this.preloader.removeClass('active')

            this.wrap.append(array);

            if (isGoods) {
                this.showPreloader = false
                this.setNumberElement(this.wrap.children().length);
            } else {
                this.showPreloader = true
                this.setNumberElement('');
            }
        }, 250);
    }
}

$(document).ready(function () {

    let filter = {
        form: '.js-filter-select',
        content: '.b-gallery',
        button: '.js-button-more',
        reset: '.js-reset-filter',
        btnSubmit: '.js-submit-filter'

    };
    let isMobile = false
    if (window.matchMedia('(max-width: 1024px)').matches) {
        isMobile = true
    }

    let modeFilterItem = 'item';
    let modeFilterPhoto = 'photo';

    let params = location.search.substr(1).split('&');
    let isLoadWithQuery = params[0] === '';


    // if ($('.js-filter-select--item').length) {
    //     let load = new Ajax(filter.form, filter.content, filter.button, filter.reset, filter.btnSubmit, isMobile);
    //     if (isLoadWithQuery) {
    //         load.loadFilterItemPage(modeFilterItem);
    //     } else {
    //         load.loadFilterWithQuery(modeFilterItem)
    //     }
    // }
    //
    // if ($('.js-filter-select--photo').length) {
    //     let load = new Ajax(filter.form, filter.content, filter.button, filter.reset, filter.btnSubmit, isMobile);
    //     if (isLoadWithQuery) {
    //         load.loadFilterItemPage(modeFilterPhoto);
    //     } else {
    //         load.loadFilterWithQuery(modeFilterPhoto)
    //     }
    // }

    if ($('.js-ajax-gallery').length) {
        //let load = new AjaxMore('.js-ajax-gallery', '.js-button-gallery-more')
    }

    if ($('.b-detail__material').length) {
        //let load = new AjaxHimGoods('.b-detail__material','.b-detail__aside')
    }

});
