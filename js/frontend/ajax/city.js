//const API = '/set_region.php';
const API = '//dev.ceramicgroup.ru/set_region.php';

$(document).ready(function() {
    var leftMap;
    var rightMap;
    var placemark_1;
    var placemark_2;

    if ($('#map-left').length > 0) {
        ymaps.ready(function() {
            leftMap = new ymaps.Map('map-left', {
                center  : window.map_office,
                zoom    : 15,
                controls: []
            });

            placemark_1 = new ymaps.Placemark(leftMap.getCenter(), {});
            leftMap.geoObjects.add(placemark_1);
        });
    }

    if ($('#map-right').length > 0) {
        ymaps.ready(function() {
            rightMap = new ymaps.Map('map-right', {
                center  : window.map_stock,
                zoom    : 15,
                controls: []
            });

            placemark_2 = new ymaps.Placemark(rightMap.getCenter(), {});
            rightMap.geoObjects.add(placemark_2);
        });
    }

    function setCookie(name, value) {
        document.cookie = name + '=' + value;
    }

    $('.js-select-city').click(function(e) {
        $('.js-select-city').removeClass('active');
        e.preventDefault();
        let $this = $(this);
        $('.b-header__select-city').removeClass('active');

        let reg_id = $this.data('reg_id');
        setCookie('region_setting_id', `${reg_id}`);
        let contact_map_stock_list = $('#contact_map_stock_list');// регион
        let contact_map_office_list = $('#contact_map_office_list');// офис

        contact_map_stock_list.fadeOut('fast');
        contact_map_office_list.fadeOut('fast');

        $.getJSON(API, {reg_id: reg_id}, function(rx) {
            contact_map_office_list.html('');

            $.each(rx, function(index, reg) {
                var cl = '';
                $('.b-header__city').text(`${reg.region_setting_city_name}`);
                $('.select-city__name span').text(`${reg.region_setting_city_name}`);
                $('.b-block__map-city').text(`${reg.region_setting_city_name}`);
                if (reg.region_setting_city_calltrack_roistat != '0') {
                    if (reg.region_setting_city_id == '1') {
                        $('.b-block__map-phone').removeClass('rst_stroeher').addClass('rst_stroeher_msk');
                        $('.b-header__phone--main').removeClass('rst_stroeher').addClass('rst_stroeher_msk');
                        $('.b-header__phone--child').removeClass('rst_stroeher_msk');
                        window.roistatCalltrackingScripts = [reg.region_setting_city_calltrack_roistat];
                    } else {
                        $('.b-block__map-phone').addClass('rst_stroeher').removeClass('rst_stroeher_msk');
                        $('.b-header__phone--main').addClass('rst_stroeher').removeClass('rst_stroeher_msk');
                        $('.b-header__phone--child').addClass('rst_stroeher_msk');
                        window.roistatCalltrackingScripts = [7, reg.region_setting_city_calltrack_roistat];
                    }
                } else {
                    $('.b-block__map-phone').removeClass('rst_stroeher').removeClass('rst_stroeher_msk');
                    $('.b-header__phone--main').removeClass('rst_stroeher').removeClass('rst_stroeher_msk');
                    $('.b-header__phone--child').removeClass('rst_stroeher').removeClass('rst_stroeher_msk');
                    if (reg.region_setting_city_id != '1') {
                        $('.b-header__phone--child').addClass('rst_stroeher_msk');
                    }
                    window.roistatCalltrackingScripts = [7];
                }
                $('.b-block__map-phone').text(`${reg.region_setting_city_phone}`);
                $('.b-header__phone--main').text(`${reg.region_setting_city_phone}`);
                if (reg.region_setting_city_id != '1') {
                    $('.b-header__phone--child').text(`${reg.region_setting_city_phone_main}`);
                } else {
                    $('.b-header__phone--child').text('');
                }
                $('.b-block__map-email').text(`${reg.region_setting_city_email}`).attr('href', `mailto:${reg.region_setting_city_email}`);
                // region_setting_city_phone_main
                if (reg.region_setting_office_coord_map == '') {
                    contact_map_office_list.append('<div>' + reg.region_setting_office_address + '</div>');
                } else {
                    contact_map_office_list.append('<div>' + reg.region_setting_office_address + ' <a href="#" data-coord_map="' + reg.region_setting_office_coord_map + '" data-coord_marker="' + reg.region_setting_office_coord_marker + '"> [ПОКАЗАТЬ НА КАРТЕ]</a></div>');
                }
                if (index == 0) {
                    $('#contact_map_stock_list h4').html(reg.region_setting_stock_name);
                    $('#contact_map_stock_list span').html(reg.region_setting_stock_address);
                    $('#contact_map_stock_list a').data('coord_map', reg.region_setting_stock_coord_map);
                    $('#contact_map_stock_list a').data('coord_marker', reg.region_setting_stock_coord_marker);
                }
            });

            if (typeof roistatCallTrackingRefresh !== 'undefined') {
                roistatCallTrackingRefresh();
            }

            contact_map_office_list.fadeIn('fast');
            contact_map_stock_list.fadeIn('fast');

            $('a[data-reg_id="' + rx[0].region_setting_city_id + '"]').addClass('active');
            $('span[data-reg_id="' + rx[0].region_setting_city_id + '"]').parent().addClass('b-popup__city-item--active');
            var coords = rx[0].region_setting_office_coord_map.trim().split(',');

            if (leftMap) {
                leftMap.panTo([coords]);
            }


            if (rx[0].region_setting_office_coord_marker != '') {
                coords = rx[0].region_setting_office_coord_marker.split(',');
            }
            if (placemark_1) {
                placemark_1.geometry.setCoordinates(coords);
            }

            coords = rx[0].region_setting_stock_coord_map.trim().split(',');
            if (rightMap) {
                rightMap.setCenter(coords);
            }
            if (rx[0].region_setting_stock_coord_marker != '') {
                coords = rx[0].region_setting_stock_coord_marker.split(',');
            }
            if (placemark_2) {
                placemark_2.geometry.setCoordinates(coords);
            }
        });
        return false;
    });

    $('.js-change-city').change(function(e) {
        e.preventDefault();
        let $this = $(this);

        let reg_id = $this.val();
        setCookie('region_setting_id', `${reg_id}`);
        let contact_map_stock_list = $('#contact_map_stock_list');// регион
        let contact_map_office_list = $('#contact_map_office_list');// офис

        contact_map_stock_list.fadeOut('fast');
        contact_map_office_list.fadeOut('fast');

        $.getJSON(API, {reg_id: reg_id}, function(rx) {
            contact_map_office_list.html('');

            $.each(rx, function(index, reg) {
                var cl = '';
                $('.b-header__city').text(`${reg.region_setting_city_name}`);
                $('.select-city__name span').text(`${reg.region_setting_city_name}`);
                $('.b-block__map-city').text(`${reg.region_setting_city_name}`);
                if (reg.region_setting_city_calltrack_roistat != '0') {
                    if (reg.region_setting_city_id == '1') {
                        $('.b-block__map-phone').removeClass('rst_stroeher').addClass('rst_stroeher_msk');
                        $('.b-header__phone--main').removeClass('rst_stroeher').addClass('rst_stroeher_msk');
                        $('.b-header__phone--child').removeClass('rst_stroeher_msk');
                        window.roistatCalltrackingScripts = [reg.region_setting_city_calltrack_roistat];
                    } else {
                        $('.b-block__map-phone').addClass('rst_stroeher').removeClass('rst_stroeher_msk');
                        $('.b-header__phone--main').addClass('rst_stroeher').removeClass('rst_stroeher_msk');
                        $('.b-header__phone--child').addClass('rst_stroeher_msk');
                        window.roistatCalltrackingScripts = [7, reg.region_setting_city_calltrack_roistat];
                    }
                } else {
                    $('.b-block__map-phone').removeClass('rst_stroeher').removeClass('rst_stroeher_msk');
                    $('.b-header__phone--main').removeClass('rst_stroeher').removeClass('rst_stroeher_msk');
                    $('.b-header__phone--child').removeClass('rst_stroeher').removeClass('rst_stroeher_msk');
                    if (reg.region_setting_city_id != '1') {
                        $('.b-header__phone--child').addClass('rst_stroeher_msk');
                    }
                    window.roistatCalltrackingScripts = [7];
                }
                $('.b-block__map-phone').text(`${reg.region_setting_city_phone}`);
                $('.b-header__phone--main').text(`${reg.region_setting_city_phone}`);
                if (reg.region_setting_city_id != '1') {
                    $('.b-header__phone--child').text(`${reg.region_setting_city_phone_main}`);
                } else {
                    $('.b-header__phone--child').text('');
                }
                $('.b-block__map-email').text(`${reg.region_setting_city_email}`).attr('href', `mailto:${reg.region_setting_city_email}`);
                // region_setting_city_phone_main
                if (reg.region_setting_office_coord_map == '') {
                    contact_map_office_list.append('<div>' + reg.region_setting_office_address + '</div>');
                } else {
                    contact_map_office_list.append('<div>' + reg.region_setting_office_address + ' <a href="#" data-coord_map="' + reg.region_setting_office_coord_map + '" data-coord_marker="' + reg.region_setting_office_coord_marker + '"> [ПОКАЗАТЬ НА КАРТЕ]</a></div>');
                }
                if (index == 0) {
                    $('#contact_map_stock_list h4').html(reg.region_setting_stock_name);
                    $('#contact_map_stock_list span').html(reg.region_setting_stock_address);
                    $('#contact_map_stock_list a').data('coord_map', reg.region_setting_stock_coord_map);
                    $('#contact_map_stock_list a').data('coord_marker', reg.region_setting_stock_coord_marker);
                }
            });

            if (typeof roistatCallTrackingRefresh !== 'undefined') {
                roistatCallTrackingRefresh();
            }

            contact_map_office_list.fadeIn('fast');
            contact_map_stock_list.fadeIn('fast');

            $('a[data-reg_id="' + rx[0].region_setting_city_id + '"]').addClass('active');
            $('span[data-reg_id="' + rx[0].region_setting_city_id + '"]').parent().addClass('b-popup__city-item--active');
            var coords = rx[0].region_setting_office_coord_map.trim().split(',');

            if (leftMap) {
                leftMap.panTo([coords]);
            }


            if (rx[0].region_setting_office_coord_marker != '') {
                coords = rx[0].region_setting_office_coord_marker.split(',');
            }
            if (placemark_1) {
                placemark_1.geometry.setCoordinates(coords);
            }

            coords = rx[0].region_setting_stock_coord_map.trim().split(',');
            if (rightMap) {
                rightMap.setCenter(coords);
            }
            if (rx[0].region_setting_stock_coord_marker != '') {
                coords = rx[0].region_setting_stock_coord_marker.split(',');
            }
            if (placemark_2) {
                placemark_2.geometry.setCoordinates(coords);
            }
        });
        return false;
    });

    $('#contact_map_office_list').on('click', 'a', function(e) {
        if ($(this).attr('href') === '#') {
            e.preventDefault()
        }
        if ($(this).data('coord_map') != '') {
            var coords = $(this).data('coord_map').split(',');
            leftMap.panTo([coords]);
            if ($(this).data('coord_marker') != '') {
                coords = $(this).data('coord_marker').split(',');
            }
            placemark_1.geometry.setCoordinates(coords);
            return false;
        }
    });

    $('#contact_map_stock_list').on('click', 'a', function(e) {
        if ($(this).attr('href') === '#') {
            e.preventDefault()
        }
        if ($(this).data('coord_map') != '') {
            var coords = $(this).data('coord_map').split(',');
            rightMap.panTo([coords]);
            if ($(this).data('coord_marker') != '') {
                coords = $(this).data('coord_marker').split(',');
            }
            placemark_2.geometry.setCoordinates(coords);
            return false;
        }
    });

    $('.b-block__map-city').on('click', function() {
        $('.b-block__contacts-slider').toggleClass('active')
    });
});
